## using
the main interface is very simple:
```$xslt
interface StatsDClient {
  val prefix: String
  val messages_sent: Long
  fun count(bucket: String, value: Int, sample: Float? = null)
  fun countInc(bucket: String) = count(bucket, 1)
  fun countDec(bucket: String) = count(bucket, -1)
  fun time(bucket: String, value_ms: Int, sample: Float? = null)
  fun gaugeSet(bucket: String, value: Int)
  fun gaugeMod(bucket: String, delta: Int)
  fun set(bucket: String, value: String)
  fun shutdown()
}
```

and we can grab an instance with:
```$xslt
import fledware.statsd.buildUdpClient
...
val client = buildUdpClient(host, port, prefix)
```

the client is thread safe, so a single instance for an application
will work well and can stay open for the entire life of the process.


## performance
the client buffers messages up to a configurable amount of bytes.

it will send the batch if:
* the next message would cause the buffer to overflow
* a certain amount of time passes without a commit

running tests locally, i get these timings:
* udp successfully sends 100k+ messages per second
* tcp successfully sends 240k+ messages per second

the reason this looks weird (tcp shouldn't be faster than udp) is
because we have to throttle udp so it can more reliably test. if 
someone knows how to better test this, that would be awesome!


## getting

add this to build.gradle
```$xslt
  repositories {
    ...
    maven {
      url "https://dl.bintray.com/rexfleischer/fledware"
    }
  }
  
  dependencies {
    ...
    compile group: 'fledware', name: 'kotlin-statsd-client', version: '0.0.1'
  }
```


## testing

most of the testing is just unit testing within gradle. but you can 
also do an end-to-end test by running ./integration-run.sh.

this will:
* turn on kamon/grafana_graphite for grafana, graphite, and statsd
* upload a test dashboard
* open the browser to the dashboard (or try to)
* start a simple seeding exec, with `./gradlew runIntegrationTest` 

