package fledware.statsd

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

/**
 * start docker-compose with the config at the root, then
 * execute this to see if metrics comes in
 */
fun main(args: Array<String>) = runBlocking {
  val client = buildUdpClient("localhost", 8125, "testing.")
  arrayOf(
      launch {
        (1..1000).forEach {
          delay(100)
          client.count("count", 1)
        }
      },
      launch {
        (1..1000).forEach {
          delay(100)
          client.count("count", 1, 0.5f)
        }
      },
      launch {
        (1..1000).forEach {
          delay(250)
          client.time("timing", it, 0.5f)
        }
      },
      launch {
        (1..1000).forEach {
          delay(250)
          client.gaugeSet("gauge", it)
        }
      },
      launch {
        (1..250).forEach {
          delay(1000)
          println("running.... messages -> ${client.messages_sent}")
        }
      }
  ).forEach { it.join() }
  
  arrayOf(
      launch {
        (1..1000).forEach {
          delay(250)
          client.gaugeMod("gauge", -1)
        }
      },
      launch {
        (1..250).forEach {
          delay(1000)
          println("running.... messages -> ${client.messages_sent}")
        }
      }
  ).forEach { it.join() }
}
