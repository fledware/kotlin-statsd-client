package fledware.statsd

import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class ClientTest(client_factory: () -> StatsDClient, val prefix: String) : BaseTest() {
  companion object {
    @JvmStatic
    @Parameterized.Parameters
    fun data() = arrayOf<Array<Any?>>(
        arrayOf({ createUdpTestClient("") }, ""),
        arrayOf({ createTcpTestClient("") }, ""),
        arrayOf({ createUdpTestClient("test.") }, "test."),
        arrayOf({ createTcpTestClient("test.") }, "test.")
    )
  }
  
  val client = client_factory() as DefaultStatsDClient
  
  @After
  fun closeStuff() {
    client.shutdown()
  }
  
  @Test
  fun testCount1() {
    client.count("oking", 234)
    Assert.assertEquals("${prefix}oking:234|c", nextMessageWait(1000L))
  }
  
  @Test
  fun testCount2() {
    client.count("oking", 234, 1.0f)
    Assert.assertEquals("${prefix}oking:234|c|@1.0", nextMessageWait(1000L))
  }
  
  @Test
  fun testCount3() {
    client.count("oking", 234, 0.05f)
    Assert.assertEquals("${prefix}oking:234|c|@0.05", nextMessageWait(1000L))
  }
  
  @Test
  fun testCountInc1() {
    client.countInc("oking")
    Assert.assertEquals("${prefix}oking:1|c", nextMessageWait(1000L))
  }
  
  @Test
  fun testCountDec1() {
    client.countDec("oking")
    Assert.assertEquals("${prefix}oking:-1|c", nextMessageWait(1000L))
  }
  
  @Test
  fun testTiming1() {
    client.time("hello", 123)
    Assert.assertEquals("${prefix}hello:123|ms", nextMessageWait(1000L))
  }
  
  @Test
  fun testTiming2() {
    client.time("hello", 123, 0.25f)
    Assert.assertEquals("${prefix}hello:123|ms|@0.25", nextMessageWait(1000L))
  }
  
  @Test
  fun testTiming3() {
    client.time("hello", 123, 0.1f)
    Assert.assertEquals("${prefix}hello:123|ms|@0.1", nextMessageWait(1000L))
  }
  
  @Test
  fun testGaugeSet1() {
    client.gaugeSet("lala", 456)
    Assert.assertEquals("${prefix}lala:456|g", nextMessageWait(1000L))
  }
  
  @Test
  fun testGaugeMod1() {
    client.gaugeMod("lala", 456)
    Assert.assertEquals("${prefix}lala:+456|g", nextMessageWait(1000L))
  }
  
  @Test
  fun testGaugeMod2() {
    client.gaugeMod("lala", -456)
    Assert.assertEquals("${prefix}lala:-456|g", nextMessageWait(1000L))
  }
  
  @Test
  fun testSet1() {
    client.set("blah", "1234")
    Assert.assertEquals("${prefix}blah:1234|s", nextMessageWait(1000L))
  }
  
  @Test
  fun testRawSend1() {
    client.send("haha", "someval", MetricTypes.TIMING)
    Assert.assertEquals("${prefix}haha:someval|ms", nextMessageWait(1000L))
  }
  
  @Test
  fun testRawSend2() {
    client.send("haha", "someval", MetricTypes.TIMING, rate = 1f)
    Assert.assertEquals("${prefix}haha:someval|ms|@1.0", nextMessageWait(1000L))
  }
  
  @Test
  fun testRawSend3() {
    client.send("haha", "someval", MetricTypes.TIMING, prefix_override = "dodo.", rate = 1f)
    Assert.assertEquals("dodo.haha:someval|ms|@1.0", nextMessageWait(1000L))
  }
  
}
