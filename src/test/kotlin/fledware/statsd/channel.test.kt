package fledware.statsd

import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.TimeoutException

class TestBufferThingy : BufferingSender() {
  val packets = ConcurrentLinkedDeque<String>()
  val messages = ConcurrentLinkedDeque<String>()
  
  override suspend fun flushBuffer(buffer: ByteBuffer): Int {
    val raw = String(buffer.array(), 0, buffer.remaining())
    packets += raw
    messages += raw.split("\n").map { it.trim() }.filter { it.isNotEmpty() }
    return buffer.remaining()
  }
  
  override fun transportShutdown() {
  
  }
  
  fun nextMessage(): String? = messages.poll()
  
  fun nextMessageWait(timeout: Long): String = pullUntilOn(messages, timeout)
  
  fun nextPacket(): String? = packets.poll()
  
  fun nextPacketWait(timeout: Long): String = pullUntilOn(packets, timeout)
  
  private fun pullUntilOn(queue: ConcurrentLinkedDeque<String>, timeout: Long): String {
    val timeout_at = System.currentTimeMillis() + timeout
    while (timeout_at > System.currentTimeMillis()) {
      val check = queue.poll()
      if (check != null) return check
      Thread.sleep(50)
    }
    throw TimeoutException()
  }
}

class ChannelSendTest {
  
  val client = TestBufferThingy()
  
  @After
  fun closeStuff() {
    client.shutdown()
  }
  
  @Test
  fun testSimpleSend() {
    client.send("hello world")
    Assert.assertEquals("hello world", client.nextMessageWait(1000L))
  }
  
  @Test
  fun testSimpleSend2() {
    client.send("hello", "world")
    Assert.assertEquals("hello", client.nextMessageWait(1000L))
    Assert.assertEquals("world", client.nextMessageWait(1000L))
  }
  
  @Test
  fun testBufferPreCommitSend() {
    client.send(
        "hello01-------------------------------------------",
        "hello02-------------------------------------------",
        "hello03-------------------------------------------",
        "hello04-------------------------------------------",
        "hello05-------------------------------------------",
        "hello06-------------------------------------------",
        "hello07-------------------------------------------",
        "hello08-------------------------------------------",
        "hello09-------------------------------------------",
        "hello10-------------------------------------------",
        "hello11-------------------------------------------",
        "hello12-------------------------------------------",
        "hello13-------------------------------------------",
        "hello14-------------------------------------------",
        "hello15-------------------------------------------"
    )
    (1..15).forEach {
      val check = it.toString().padStart(2, '0')
      Assert.assertEquals(
          "hello$check-------------------------------------------",
          client.nextMessageWait(1000L)
      )
    }
    
    val first_packet = (1..10).joinToString(separator = "\n") {
      val check = it.toString().padStart(2, '0')
      "hello$check-------------------------------------------"
    } + "\n"
    Assert.assertEquals(first_packet, client.nextPacket())

    val second_packet = (11..15).joinToString(separator = "\n") {
      "hello$it-------------------------------------------"
    } + "\n"
    Assert.assertEquals(second_packet, client.nextPacket())
  }
}
