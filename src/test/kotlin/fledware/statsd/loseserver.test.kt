package fledware.statsd

import org.junit.After
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test

class LoseServerTest : BaseTest() {
  
  lateinit var client: StatsDClient
  
  @After
  fun closeStuff() {
    client.shutdown()
  }
  
  @Test
  fun testUdpLoseServer() {
    client = createUdpTestClient("")
  
    client.count("oking", 123)
    Assert.assertEquals("oking:123|c", nextMessageWait(1000L))
  
    TestServer.ensureShutdown()
    Thread.sleep(300)
  
    Assert.assertNull(nextMessage())
    (1..9).forEach { client.count("oking", it) }
    
    Thread.sleep(300)
    Assert.assertNull(nextMessage())
    client.count("oking", 2345)
    client.count("oking", 2345)
    Thread.sleep(300)
  
    TestServer.ensureStart()
    Thread.sleep(300)
    Assert.assertNull(nextMessage())
  
    client.count("oking", 34567)
    Assert.assertEquals("oking:34567|c", nextMessageWait(1000L))
  }
  
  @Test
  @Ignore("doesnt work because it the client doesnt reconnect if the socket dies")
  fun testTcpLoseServer() {
    client = createTcpTestClient("")
    
    client.count("oking", 123)
    Assert.assertEquals("oking:123|c", nextMessageWait(1000L))
    Assert.assertNull(nextMessage())
  
    TestServer.ensureShutdown()
    Thread.sleep(300)
    (1..9).forEach { client.count("oking", it) }
    Thread.sleep(300)
    Assert.assertNull(nextMessage())
  
    TestServer.ensureStart()
    Thread.sleep(300)
    Assert.assertNull(nextMessage())
  
    client.count("oking", 34567)
    Assert.assertEquals("oking:34567|c", nextMessageWait(1000L))
  }
}
