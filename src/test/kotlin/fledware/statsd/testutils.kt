package fledware.statsd

import org.junit.After
import org.junit.Assert
import org.junit.Before
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousCloseException
import java.nio.channels.ClosedChannelException
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel
import java.util.Deque
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.TimeoutException
import kotlin.concurrent.thread

fun createUdpTestClient(prefix: String) = buildUdpClient("localhost",
    TestServer.UDP_PORT, prefix, commit_interval = 50, failed_send_attempt_max = 20, failed_send_attempt_delay = 100)

fun createTcpTestClient(prefix: String) = buildTcpClient("localhost", TestServer.TCP_PORT, prefix, commit_interval = 50)

abstract class BaseTest {
  @Before
  fun clearStuff() {
    TestServer.ensureStart()
    TestServer.clear()
  }
  
  fun messageCount(): Int = TestServer.messages.size
  fun nextMessage(): String? = TestServer.nextMessage()
  fun nextMessageWait(timeout: Long): String = TestServer.nextMessageWait(timeout)
  fun assertServerEmpty() = TestServer.assertEmpty()
}

class LineBufferAppender(private val target: MutableCollection<String>) {
  private val char_buffer = CharArray(1024)
  private var char_at = 0
  
  fun bufferStuff(next: CharSequence) {
    next.forEach {
      when (it) {
        '\n' -> {
          if (char_at != 0) target.add(String(char_buffer, 0, char_at))
          char_at = 0
        }
        else -> {
          char_buffer[char_at++] = it
        }
      }
    }
  }
}

private class UdpServer(target: Deque<String>) {
  private val server: DatagramSocket = DatagramSocket(TestServer.UDP_PORT)
  private val strings = LineBufferAppender(target)
  private val udp_thread = thread(isDaemon = true) {
    println("udp server starting")
    while (!server.isClosed) {
      try {
        val packet = DatagramPacket(ByteArray(1500), 1500)
        server.receive(packet)
        val raw = String(packet.data, 0, packet.length)
        strings.bufferStuff(raw)
      }
      catch (ex: IOException) {
        if (ex.message == "Socket closed") break
        ex.printStackTrace()
      }
    }
    println("udp server ending")
  }
  
  fun shutdown() {
    try {
      server.close()
    }
    catch (ex: Exception) {
    }
  }
}

private class TcpServer(private val target: Deque<String>) {
  private val server: ServerSocketChannel = ServerSocketChannel.open().bind(InetSocketAddress(TestServer.TCP_PORT))
  private val children = ArrayList<SocketChannel>()
  private val tcp_thread = thread(isDaemon = true) {
    while (!server.socket().isClosed) {
      try {
        val incoming = server.accept()
        children += incoming
        openChild(incoming)
      }
      catch (ex: IOException) {
        if (ex is AsynchronousCloseException) break
        ex.printStackTrace()
      }
    }
  }
  
  private fun openChild(socket: SocketChannel) = thread(isDaemon = true) {
    val strings = LineBufferAppender(target)
    val buffer = ByteBuffer.allocate(1024)
    socket.use { socket ->
      while (socket.isOpen) {
        buffer.clear()
        try {
          socket.read(buffer)
        }
        catch (ex: IOException) {
          if (!(ex is AsynchronousCloseException || ex is ClosedChannelException)) ex.printStackTrace()
          return@use
        }
        val raw = String(buffer.array(), 0, buffer.position())
        strings.bufferStuff(raw)
      }
    }
  }
  
  fun shutdown() {
    try {
      server.close()
      children.forEach { it.close() }
    }
    catch (ex: Exception) {
      ex.printStackTrace()
    }
  }
}

object TestServer {
  const val UDP_PORT = 12345
  const val TCP_PORT = 12346
  
  val messages = ConcurrentLinkedDeque<String>()
  
  private var udp: UdpServer? = UdpServer(messages)
  private var tcp: TcpServer? = TcpServer(messages)
  
  init {
    ensureStart()
    Runtime.getRuntime().addShutdownHook(Thread { ensureShutdown() })
  }
  
  fun ensureShutdown() {
    udp?.shutdown()
    udp = null
    tcp?.shutdown()
    tcp = null
  }
  
  fun ensureStart() {
    val udp_check = udp
    if (udp_check == null) udp = UdpServer(messages)
    val tcp_check = tcp
    if (tcp_check == null) tcp = TcpServer(messages)
  }
  
  fun nextMessage(): String? = messages.poll()
  
  fun nextMessageWait(timeout: Long): String = pullUntilOn(messages, timeout)
  
  fun assertEmpty() {
    Assert.assertTrue(messages.isEmpty())
  }
  
  private fun pullUntilOn(queue: ConcurrentLinkedDeque<String>, timeout: Long): String {
    val timeout_at = System.currentTimeMillis() + timeout
    while (timeout_at > System.currentTimeMillis()) {
      val check = queue.poll()
      if (check != null) return check
      Thread.sleep(50)
    }
    throw TimeoutException()
  }
  
  
  fun clear() {
    messages.clear()
  }
}
