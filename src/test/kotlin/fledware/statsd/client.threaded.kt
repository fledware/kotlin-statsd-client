package fledware.statsd

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.junit.After
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import kotlin.system.measureTimeMillis

@Ignore("usually works, but sometimes fails by dropped packets")
class ClientThreadedTest : BaseTest() {
  
  lateinit var client: StatsDClient
  val allowed_time = 20_000
  val jobs_count = 1_000
  val job_message_count = 1_000
  val total_messages = jobs_count * job_message_count
  
  @After
  fun closeStuff() {
    client.shutdown()
  }
  
  @Test
  fun testUdpConcurrent() {
    client = createUdpTestClient("test.")
  
    runTestOnClient(true)
  }
  
  @Test
  fun testTcpConcurrent() {
    client = createTcpTestClient("test2.")
  
    runTestOnClient(false)
  }
  
  private fun runTestOnClient(do_delay: Boolean) {
    val time = measureTimeMillis {
      (1..jobs_count).map { number ->
        launch {
          (1..job_message_count).forEachIndexed { index, iteration ->
            client.time("somebucket", number * iteration)
            if (do_delay) {
              // we have to delay because... well, its udp and we will lose
              // packets if we go too fast.
              // if someone has a better understanding of making udp more testable,
              // please, do a PR or let me know
              delay(1)
            }
          }
        }
      }
    
      var last_count = messageCount()
      val timeout = System.currentTimeMillis() + allowed_time
      while (timeout > System.currentTimeMillis() && messageCount() < total_messages) {
        println("count: ${messageCount()}")
        Thread.sleep(500)
  
        if (last_count == messageCount()) {
          println("messages stopped being received: $last_count == ${messageCount()}")
          Assert.fail()
        }
        last_count = messageCount()
      }
    }
  
    println("${messageCount()} messages in $time ms")
    val mpms = messageCount().toDouble() / time.toDouble()
    println("that's ${mpms.toInt()} m/ms")
    println("that's ${(mpms * 1000).toInt()} m/s")
    Assert.assertEquals(total_messages, messageCount())
    Assert.assertTrue(time < allowed_time)
  }
}
