package fledware.statsd

/**
 * the main interface to interact with the client
 */
interface StatsDClient {
  
  val prefix: String
  
  val messages_sent: Long
  
  fun count(bucket: String, value: Int, sample: Float? = null)
  
  fun countInc(bucket: String) = count(bucket, 1)
  
  fun countDec(bucket: String) = count(bucket, -1)
  
  fun time(bucket: String, value_ms: Int, sample: Float? = null)
  
  fun gaugeSet(bucket: String, value: Int)
  
  fun gaugeMod(bucket: String, delta: Int)
  
  fun set(bucket: String, value: String)
  
  fun shutdown()
}

/**
 * the default stats client
 */
class DefaultStatsDClient(private val sender: StatsSender,
                          override val prefix: String = "",
                          private val formatter: StatsDFormatter = default_formatter) : StatsDClient {
  
  override val messages_sent: Long get() = sender.messages_sent
  
  override fun count(bucket: String, value: Int, sample: Float?) {
    sender.send(formatter(prefix, bucket, value.toString(), MetricTypes.COUNT, sample))
  }
  
  override fun time(bucket: String, value_ms: Int, sample: Float?) {
    sender.send(formatter(prefix, bucket, value_ms.toString(), MetricTypes.TIMING, sample))
  }
  
  override fun gaugeSet(bucket: String, value: Int) {
    if (value < 0) throw IllegalArgumentException("gauge set cannot be negative: $bucket -> $value")
    sender.send(formatter(prefix, bucket, value.toString(), MetricTypes.GAUGE, null))
  }
  
  override fun gaugeMod(bucket: String, delta: Int) {
    val value = "${if (delta > 0) "+" else "-"}${Math.abs(delta)}"
    sender.send(formatter(prefix, bucket, value, MetricTypes.GAUGE, null))
  }
  
  override fun set(bucket: String, value: String) {
    sender.send(formatter(prefix, bucket, value, MetricTypes.SET, null))
  }
  
  override fun shutdown() {
    sender.shutdown()
  }
  
  fun send(bucket: String, value: String, type: MetricTypes, prefix_override: String? = null, rate: Float? = null) {
    val sending_prefix = prefix_override ?: prefix
    sender.send(formatter(sending_prefix, bucket, value, type, rate))
  }
}
