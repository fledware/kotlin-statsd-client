@file:Suppress("EXPERIMENTAL_FEATURE_WARNING")

package fledware.statsd

import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.sendBlocking
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.future
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import java.io.IOException
import java.net.SocketAddress
import java.nio.ByteBuffer
import java.nio.channels.DatagramChannel
import java.nio.channels.SocketChannel
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

interface StatsSender {
  val messages_sent: Long
  fun send(vararg values: String)
  fun shutdown()
}

/**
 * the base sender for doing efficient message buffering
 */
abstract class BufferingSender(max_send_size: Int = 512,
                               inbound_channel_size: Int = 10_000,
                               commit_interval: Long = 250) : StatsSender {
  companion object {
    private val logger = LoggerFactory.getLogger(DatagramChannelSender::class.java)
    private const val commit = "_c_"
    private const val new_line = '\n'.toByte()
  }
  
  private val _messages_sent = AtomicLong(0)
  private val buffer = ByteBuffer.allocate(max_send_size)
  private val inbound = Channel<String>(inbound_channel_size)
  
  override val messages_sent: Long get() = _messages_sent.get()
  
  final override fun send(vararg values: String) {
    values.forEach { inbound.sendBlocking(it) }
  }
  
  private val commit_notifier_job = launch {
    while (isActive) {
      delay(commit_interval, TimeUnit.MILLISECONDS)
      inbound.send(commit)
    }
  }
  
  private val sender_job = launch {
    while (isActive) {
      val message = inbound.receiveOrNull() ?: break
      if (message == commit) {
        readyAndSendFlush()
      }
      else {
        val data = message.toByteArray()
        if (data.size + 1 > buffer.remaining()) {
          readyAndSendFlush()
        }
        buffer.put(data)
        buffer.put(new_line)
        _messages_sent.incrementAndGet()
      }
    }
  }
  
  private suspend fun readyAndSendFlush() {
    val size_check = buffer.position()
    if (size_check == 0) return
    buffer.flip()
    val sent_bytes = flushBuffer(buffer)
    if (sent_bytes != size_check) {
      logger.warn("not all bytes sent")
    }
    buffer.clear()
  }
  
  final override fun shutdown() {
    try {
      listOf(
          future { commit_notifier_job.cancelAndJoin() },
          future { sender_job.cancelAndJoin() }
      ).forEach { it.join() }
    }
    catch (ex: Exception) {
      logger.warn("error during job shutdown", ex)
    }
    
    inbound.close()
    
    transportShutdown()
  }
  
  protected abstract suspend fun flushBuffer(buffer: ByteBuffer): Int
  
  protected abstract fun transportShutdown()
}

/**
 * the channel to use for udp sending
 */
class DatagramChannelSender(address: SocketAddress,
                            max_send_size: Int = 512,
                            inbound_channel_size: Int = 10_000,
                            commit_interval: Long = 250,
                            private val failed_send_attempt_max: Int = 3,
                            private val failed_send_attempt_delay: Long = 1_000) :
    BufferingSender(max_send_size, inbound_channel_size, commit_interval) {
  companion object {
    private val logger = LoggerFactory.getLogger(DatagramChannelSender::class.java)
  }
  
  private val channel = DatagramChannel.open().connect(address)
  
  override suspend fun flushBuffer(buffer: ByteBuffer): Int {
    buffer.mark()
    (1..failed_send_attempt_max).forEach {
      try {
        return channel.write(buffer)
      }
      catch (ex: IOException) {
        logger.warn("error during datagram write: ${ex::class.java} -> ${ex.message}")
        delay(failed_send_attempt_delay)
        buffer.rewind()
      }
    }
    return 0
  }
  
  override fun transportShutdown() {
    try {
      channel.close()
    }
    catch (ex: Exception) {
      logger.warn("error during datagram close", ex)
    }
  }
}

/**
 * the channel to use for tcp sending
 */
class SocketChannelSender(address: SocketAddress,
                          max_send_size: Int = 512,
                          inbound_channel_size: Int = 10_000,
                          commit_interval: Long = 250) :
    BufferingSender(max_send_size, inbound_channel_size, commit_interval) {
  companion object {
    private val logger = LoggerFactory.getLogger(SocketChannelSender::class.java)
  }
  
  private val channel = SocketChannel.open(address)
  
  override suspend fun flushBuffer(buffer: ByteBuffer): Int {
    // TODO: this needs some type of reconnect logic. if a tcp socket
    // TODO: is open too long, or something, then we may get closed errors
    // TODO: and whatnot and should try to reconnect
    return channel.write(buffer)
  }
  
  override fun transportShutdown() {
    try {
      channel.close()
    }
    catch (ex: Exception) {
      logger.warn("error during datagram close", ex)
    }
  }
}

