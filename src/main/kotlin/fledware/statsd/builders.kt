package fledware.statsd

import java.net.InetSocketAddress

fun buildUdpClient(hostname: String, port: Int,
                   bucket_prefix: String = "",
                   max_send_size: Int = 512,
                   inbound_channel_size: Int = 10_000,
                   commit_interval: Long = 250,
                   failed_send_attempt_max: Int = 3,
                   failed_send_attempt_delay: Long = 1_000,
                   formatter: StatsDFormatter = default_formatter): StatsDClient = DefaultStatsDClient(
    sender = DatagramChannelSender(
        address = InetSocketAddress(hostname, port),
        max_send_size = max_send_size,
        inbound_channel_size = inbound_channel_size,
        commit_interval = commit_interval,
        failed_send_attempt_max = failed_send_attempt_max,
        failed_send_attempt_delay = failed_send_attempt_delay
    ),
    prefix = bucket_prefix,
    formatter = formatter
)

/**
 * this is not a reliable client yet. if the socket fails, then
 * it will not recover correctly.
 */
fun buildTcpClient(hostname: String, port: Int,
                   bucket_prefix: String = "",
                   max_send_size: Int = 512,
                   inbound_channel_size: Int = 10_000,
                   commit_interval: Long = 250,
                   formatter: StatsDFormatter = default_formatter): StatsDClient = DefaultStatsDClient(
    sender = SocketChannelSender(
        address = InetSocketAddress(hostname, port),
        max_send_size = max_send_size,
        inbound_channel_size = inbound_channel_size,
        commit_interval = commit_interval
    ),
    prefix = bucket_prefix,
    formatter = formatter
)
