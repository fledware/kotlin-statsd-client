package fledware.statsd

/**
 * the known types
 */
enum class MetricTypes(val type: String) {
  TIMING("ms"),
  COUNT("c"),
  GAUGE("g"),
  SET("s");
}

/**
 * the formatter type
 */
typealias StatsDFormatter = (prefix: String, key: String, value: String, type: MetricTypes, rate: Float?) -> String

/**
 * the default formatter
 */
val default_formatter: StatsDFormatter = { prefix, key, value, type, rate ->
  val result = StringBuilder(prefix).append(key).append(":").append(value).append("|").append(type.type)
  if (rate != null) result.append("|@").append(rate)
  result.toString()
}
