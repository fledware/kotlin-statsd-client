#!/usr/bin/env bash

set -x
docker-compose down -v

set -e
rm -r ./.integration/
mkdir -p ./.integration/data/elasticsearch
mkdir -p ./.integration/data/grafana
mkdir -p ./.integration/data/whisper
mkdir -p ./.integration/log/elasticsearch
mkdir -p ./.integration/log/graphite/webapp
mkdir -p ./.integration/log/supervisor

docker-compose up -d

set +x

sleep 10
curl -X POST --data "@data/test-dashboard.json" -H "Content-Type: application/json" http://admin:admin@localhost/api/dashboards/db
echo ""
echo "============== "
echo "ready to go: http://localhost/dashboard/db/integration-test-dashboard?refresh=5s&orgId=1"
echo "user: admin"
echo "pass: admin"
echo "opening...."
sleep 3

open http://localhost/dashboard/db/integration-test-dashboard?refresh=5s&orgId=1

sleep 3

./gradlew runIntegrationTest
